from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view
from rest_framework import renderers
from rest_framework.schemas import get_schema_view
from fruit import views
from fruit.views import FruitViewSet, UserViewSet

fruit_list = FruitViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
fruit_detail = FruitViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destory'
})
user_list=UserViewSet.as_view({
    'get': 'list'
})
user_detail=UserViewSet.as_view({
    'get': 'retrieve'
})


urlpatterns = format_suffix_patterns([
    url(r'^fruit/$', fruit_list, name='fruit-list'),
    url(r'^fruit/(?P<pk>[0-9]+)/$', fruit_detail, name='fruit-detail'),
    url(r'^user/$', user_list, name='user-list'),
    url(r'^user/(?P<pk>[0-9]+)/$', user_detail, name='user-detail'),
])

urlpatterns = format_suffix_patterns(urlpatterns)
urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls')),
]

router = DefaultRouter()
router.register(r'fruit', views.FruitViewSet)
router.register(r'user', views.UserViewSet)

schema_view = get_schema_view(title='Fruit API')
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^schema/$', schema_view),
]