from rest_framework import serializers
from fruit.models import Categories
from django.contrib.auth.models import User

class FruitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = ('id', 'categories_test')
        owner = serializers.ReadOnlyField(source='owner.username')

class UserSerializer(serializers.HyperlinkedModelSerializer):
#    fruit= serializers.PrimaryKeyRelatedField(many=True, queryset=Categories.objects.all())
    #fruits = serializers.HyperlinkedRelatedField(many=True, view_name='fruit-detail', read_only=True)
    class Meta:
        model = User
        fields = ('id','username','fruit')
""" id = serializers.IntegerField(read_only=True)
    categories_test = serializers.CharField(max_length=100)
    def create(self, validated_data):
        return Categories.object.create(**validated_data)

    def update(self, instance, validated_data):
        instance.categories_test = validated_data.get('categories_test', instance.categories_test)
        instance.save()
        return instance
"""

