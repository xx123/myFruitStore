from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework import renderers
from rest_framework.reverse import reverse
from rest_framework.response import Response
from fruit.serializer import FruitSerializer, UserSerializer
from fruit.models import Categories
from django.contrib.auth.models import User
from rest_framework import permissions
from fruit.permissions import IsOwnerOrReadOnly
from rest_framework import viewsets
from rest_framework.decorators import detail_route



class FruitViewSet(viewsets.ModelViewSet):
    queryset = Categories.objects.all()
    serializer_class = FruitSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer




