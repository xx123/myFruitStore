from django.db import models
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
# Create your models here.


class Categories(models.Model):
    categories_test = models.CharField(max_length=200)
    owner = models.ForeignKey('auth.User', related_name='fruit', on_delete=models.CASCADE)
    def save(self, *args, **kwargs):
        options = self.categories_test and {'categories_test': self.categories_test} or {}
        super(Categories, self).save(*args, **kwargs)
    class Meta:
        ordering = ('categories_test',)
